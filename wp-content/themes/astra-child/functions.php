<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );
if ( current_user_can( 'manage_options' ) ) {
	add_filter( 'show_admin_bar', '__return_true' );
}

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
	wp_enqueue_style( 'style-name', get_stylesheet_directory_uri()."/base.min.css", array(), "", false);
	wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
    wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
    
 
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

/*
    Gravity Forms => 1.9
*/

add_filter('gform_form_args', 'no_ajax_on_all_forms', 10, 1);
function no_ajax_on_all_forms($args){
    $args['ajax'] = false;
    return $args;
}

function year_shortcode() {
  $year = date('Y');
  return $year;
}
add_shortcode('year', 'year_shortcode');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
  if ( isset( $atts['facet'] ) ) {       
      $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
  }
  return $output;
}, 10, 2 );




function wphe_body_classes( $classes ) {
    global $post;
     
    if ( is_singular( 'post' ) ) {  
        $classes[] = 'single';  // add custom class to all single posts
    }
     
    if ( is_singular( 'page' ) ) {  
        $classes[] = 'page';  // add custom class to all single pages
    }   
       
    if ( is_home() ) {  
        $classes[] = 'home';  // add custom class to blog homepage
    }           
                    
 
    return $classes;
}
add_filter( 'body_class', 'wphe_body_classes' );
//Custom Banner home page banner hook
function astratheme_custom_alert_banner() {  

    $website_json =  json_decode(get_option('website_json'));
     

     foreach($website_json->sites as $site_chat){

        if($site_chat->instance == 'prod'){ 

            if ( $site_chat->banner != null &&  is_front_page() ) {

                $content = '<div class="custombanneralert" style="background-color:'.$website_json->color1.';">'.$site_chat->banner.'</div>';
                
                echo $content;
            }
        }
    }
    
}
add_action( 'astra_header_after', 'astratheme_custom_alert_banner' );

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

add_filter( 'auto_update_plugin', '__return_false' );

function mmsession_custom_footer_js_astratheme() {
     
    echo "<script src='https://session.mm-api.agency/js/mmsession.js' async></script>";
}
add_action( 'wp_footer', 'mmsession_custom_footer_js_astratheme' );


//Roomvo script
add_action('astra_body_bottom', 'astra_roomvo_custom_footer_js');
function astra_roomvo_custom_footer_js() {

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){
    
  echo "<script src='https://www.roomvo.com/static/scripts/b2b/mobilemarketing.js' async></script>";

            }
        }
    }
}