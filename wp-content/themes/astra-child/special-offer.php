<?php
/*
Template Name: Special Offer Landing Page 
*/

 get_header(); 
 ?>

<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12">
		<div class="currentPromoWrap">
			<img src="https://icustomfloor.com/wp-content/uploads/2022/03/Asset-4@4x.png" alt=" Specials Offers "  />	
		</div>
		<div class="product-detail-layout-6 custom">
<?php
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');
?>
<?php
	if( have_rows('special_product_details') ):

		while( have_rows('special_product_details') ) : the_row();
			$product_title = get_sub_field('product_title');
			$product_collection = get_sub_field('product_collection');
			$color_count = count(get_sub_field('color_variation'));
			$current_product =  $product_array = array();
			if( have_rows('color_variation') ):
				$i= 1;
				
				while( have_rows('color_variation') ) : the_row();
					$color = get_sub_field('product_color');
					$product_array[$color] = array(
						'swatch' => get_sub_field('product_swatch'),
						'gallery' =>  get_sub_field('product_gallery'),
						'color' => $color
					);	
				
				
				if((isset($_GET['product']) && isset($_GET['color']) ) && ($_GET['product'] == $product_collection && $_GET['color'] == $color)){
					$current_product = array(
						'swatch' => get_sub_field('product_swatch'),
						'gallery' =>  get_sub_field('product_gallery'),
						'color' => $color
					);	
					
				}else if($i == 1){
					$current_product = array(
						'swatch' => get_sub_field('product_swatch'),
						'gallery' =>  get_sub_field('product_gallery'),
						'color' => $color
					);	
				}
					$i++;
				endwhile;
			endif;	
		    
			$image = $current_product['swatch'];
			$color = $current_product['color'];
			$product_gallery = $current_product['gallery'];
			$cur_color = $current_product['color'];
	
	?>
				<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
					<div class="fl-post-content clearfix grey-back" itemprop="text">
						<div class="clearfix"></div>
							<div class="row">
								<div class="col-md-6 col-sm-12 product-swatch">   
									<div class="imagesHolder">
										<div id="product-images-holder">
											<?php  
												if (!empty($image)){
											?>
											<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: 100% 100%;background-position:center" data-targetimg="gallery_item_0" data-responsive="<?php echo $image; ?>" data-src="<?php echo $image; ?>" data-exthumbimage="<?php echo $image; ?>">
												<a href="javascript:void(0)" class="popup-overlay-link"></a>
												<span class="main-imgs"><img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
											</div>
											<?php } else{ ?>
											<div class="img-responsive toggle-image" data-targetimg="gallery_item_0" style="background-image:url('http://placehold.it/168x123?text=COMING+SOON');background-size: cover;background-position:center">
												<a href="http://placehold.it/168x123?text=COMING+SOON" class="popup-overlay-link"></a>
												<span class="main-imgs"><img src="http://placehold.it/168x123?text=COMING+SOON" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
											</div>
											<?php } ?>
											<?php
												// check if the repeater field has rows of data
												if($product_gallery!=''){
													$gal_count = 1;
													$room_image =  $product_gallery;
													$room_image_thumb =  $product_gallery;
													?>
														<div class="popup-imgs-holder" data-targetimg="gallery_item_<?php echo $gal_count; ?>" data-responsive="<?php echo $room_image; ?>" data-exthumbimage="<?php echo $room_image_thumb; ?>" data-src="<?php echo $room_image; ?>"><a href="javascript:void(0)" title="<?php the_title_attribute(); ?>"><span class="main-imgs"><img src="<?php echo $room_image; ?>" alt="<?php the_title_attribute(); ?>" /></span></a></div>
													<?php
												}
											?>
											
										</div>
									</div>


									<?php if($product_gallery){ ?>
										<div class="toggle-image-thumbnails">
											<?php if (!empty($image)){ ?>
												<div class="toggle-image-holder"><a href="javascript:void(0)" class="active" data-targetimg="gallery_item_0" data-background="<?php echo $image; ?>"  style="background-image:url('<?php echo $image; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a></div>
											<?php } ?>       
											<?php
											// check if the repeater field has rows of data
											if($product_gallery){
												$gallery_count = 1;
													$room_image =  $product_gallery;
													$room_image_small =  $product_gallery;
											?>
												<div class="toggle-image-holder">
													<a href="javascript:void(0)" data-targetimg="gallery_item_<?php echo $gallery_count; ?>" data-background="<?php echo $room_image; ?>" data-thumbnail="<?php echo $room_image_small; ?>"  style="background-image:url('<?php echo $room_image_small; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
												</div>
											<?php
												$gallery_count++;
											}
											?>
										</div>
									<?php } ?>
									<div class="clearfix"></div>
								</div>
							<div class="col-md-5 col-sm-12 product-box">
								<div class="row">
									<div class="col-md-12">
										<?php if($product_title){ ?>
										<h4><?php echo $product_title; ?></h4>
										<?php } ?>

										<?php if($color){ ?>
										<h2 class="fl-post-title" itemprop="name"><?php  echo $color; ?></h2>
										<?php } ?>
									</div>							
									
									<div class="clearfix"></div>
								</div>
								<div class="row">
									<div class="col-md-12">	
										<div class="product-colors">
											<ul>
												<li class="found"><?php echo $color_count; ?></li>
												<li class="colors">Colors Available</li>
											</ul>
										</div>

										<div id="product-colors">
											<div class="product-variations">
												<div class="color_variations_slider_1">
													<div class="slides">
														<?php
														if( count($product_array)>0 ){
															foreach($product_array as $product_item) {
																$image = $product_item['swatch'];
																$color = $product_item['color'];
																					
																$style = "padding: 5px;";
																?>
																<div class="slide col-md-2 col-sm-3 col-xs-6 color-box <?php if((isset($GET['color']) && $color == $_GET['color']) || $color == $cur_color){ echo 'selected-slide';}?>">
																	<figure class="color-boxs-inner">
																		<div class="color-boxs-inners">
																			<a href="/specials-offers/?product=<?php echo $product_collection;?>&color=<?php echo $color;?>">
																				<img src="<?php echo $image; ?>" style="<?php echo $style; ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
																			</a>
																			
																			<div><small><?php echo $color; ?></small></div>
																		</div>
																	</figure>
																</div>
															<?php
															}
														}	
														?>
													</div>
												</div>
											</div>
										</div>
									</div>	
								</div>		


								
								<div class="button-wrapper">

									<div class="dual-button">
										<a href="/contact-us/" class="button contact-btn">CONTACT US</a>	
										<?php if($pdp_get_finance != 1 || $pdp_get_finance == '' ){?>						
											<a href="<?php if($getfinancereplace ==1){ echo $getfinancereplaceurl ;}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button"><?php if($getfinancereplace =='1'){ echo $getfinancetext ;}else{ echo 'Financing'; } ?></a>	
										<?php } ?>		
									</div>
									
									<a href="tel:+17035501200" target="_self" class="button alt getcoupon-btn" role="button" >CALL FOR PRICE</a>
								
								</div>						
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						
						<div class="clearfix"></div>
						
					</div>
				</article>
				<?php
				
			endwhile;
			else :
			endif;
	?>
</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>