<?php
/*
Template Name: Special Offer Landing Page 
*/

 get_header(); 
 ?>

<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12">
		<div class="product-detail-layout-6">
<?php
$getcouponbtn = get_option('getcouponbtn');
$getcouponreplace = get_option('getcouponreplace');
$getcouponreplacetext = get_option('getcouponreplacetext');
$getcouponreplaceurl = get_option('getcouponreplaceurl');
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');
$getcoupon_link = get_option('getcoupon_link');
?>
<?php

	// $args = array(
	// 	'post_type'      => $flooringtype,
	// 	'posts_per_page' => -1,
	// 	'post_status'    => 'publish',
	// 	'meta_query'     => array(
	// 		array(
	// 			'key'     => $key,
	// 			'value'   => $familycolor,
	// 			'compare' => '='
	// 		),
	// 		array(
	// 			'key' => 'swatch_image_link',
	// 			'value' => '',
	// 			'compare' => '!='
	// 			)
	// 	)
	// );										

	// $the_query = new WP_Query( $args );
	if( have_rows('special_product_details') ):

		while( have_rows('special_product_details') ) : the_row();

		    $main_image = "";
			// if( have_rows('color_variation') ):
			// 	while( have_rows('color_variation') ) : the_row();
			// 		$main_image = get_sub_field('product_swatch');
			// 	endwhile;
			// else :
			// endif;
	
	?>
				<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
					<div class="fl-post-content clearfix grey-back" itemprop="text">
							<div class="clearfix"></div>
								<div class="row">
									<div class="col-md-6 col-sm-12 product-swatch">   
									<?php
										$image = $main_image;
									
								?>
								<div class="imagesHolder">
									<div id="product-images-holder">
										<?php  
											if (!empty($image)){
										?>
										<div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: 100% 100%;background-position:center" data-targetimg="gallery_item_0" data-responsive="<?php echo $image; ?>" data-src="<?php echo $image; ?>" data-exthumbimage="<?php echo $image; ?>">
											<a href="javascript:void(0)" class="popup-overlay-link"></a>
											<span class="main-imgs"><img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
										</div>
										<?php } else{ ?>
										<div class="img-responsive toggle-image" data-targetimg="gallery_item_0" style="background-image:url('http://placehold.it/168x123?text=COMING+SOON');background-size: cover;background-position:center">
											<a href="http://placehold.it/168x123?text=COMING+SOON" class="popup-overlay-link"></a>
											<span class="main-imgs"><img src="http://placehold.it/168x123?text=COMING+SOON" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
										</div>
										<?php } ?>
										<?php
											// check if the repeater field has rows of data
											if(get_sub_field('product_gallery')!=''){
												$gal_count = 1;
												$room_image =  high_gallery_images_in_loop(get_sub_field('product_gallery'));
												$room_image_thumb =  thumb_gallery_images_in_loop(get_sub_field('product_gallery'));
												?>
													<div class="popup-imgs-holder" data-targetimg="gallery_item_<?php echo $gal_count; ?>" data-responsive="<?php echo $room_image; ?>" data-exthumbimage="<?php echo $room_image_thumb; ?>" data-src="<?php echo $room_image; ?>"><a href="javascript:void(0)" title="<?php the_title_attribute(); ?>"><span class="main-imgs"><img src="<?php echo $room_image; ?>" alt="<?php the_title_attribute(); ?>" /></span></a></div>
												<?php
											}
										?>
										
									</div>
										
								</div>


									<?php if(get_sub_field('product_gallery')){ ?>
										<div class="toggle-image-thumbnails">
											<?php if (!empty($image)){ ?>
												<div class="toggle-image-holder"><a href="javascript:void(0)" class="active" data-targetimg="gallery_item_0" data-background="<?php echo $image; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a></div>
											<?php } ?>       
											<?php
											// check if the repeater field has rows of data
											if(get_sub_field('product_gallery')){
												$gallery_count = 1;
													$room_image =  high_gallery_images_in_loop(get_sub_field('product_gallery'));
													$room_image_small =  thumb_gallery_images_in_loop(get_sub_field('product_gallery'));
											?>
												<div class="toggle-image-holder">
													<a href="javascript:void(0)" data-targetimg="gallery_item_<?php echo $gallery_count; ?>" data-background="<?php echo $room_image; ?>" data-thumbnail="<?php echo $room_image_small; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $room_image_small; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
												</div>
											<?php
												$gallery_count++;
											}
											?>
										</div>
									<?php } ?>
									<div class="clearfix"></div>
							</div>
							<div class="col-md-5 col-sm-12 product-box">
								<div class="row">
									<div class="col-md-12">
										<?php if(get_sub_field('product_title')){ ?>
										<h4><?php echo get_sub_field('product_title'); ?></h4>
										<?php } ?>

										<?php if(get_sub_field('product_color')){ ?>
										<h2 class="fl-post-title" itemprop="name"><?php  echo get_sub_field('product_color'); ?></h2>
										<?php } ?>
									</div>							
									
									<div class="clearfix"></div>
								</div>
								<div class="row">
									<div class="col-md-12">	
									<div class="product-colors">
											<ul>
												<li class="found"><?php echo $the_query ->found_posts; ?></li>
												<li class="colors">Colors Available</li>
											</ul>
										</div>

										<div id="product-colors">
											<?php
												// $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider-1.php';
												// include( $dir );
											?>
										</div>
									</div>	
								</div>		


								
								<div class="button-wrapper">

								<div class="dual-button">
									<a href="/contact-us/" class="button contact-btn">CONTACT US</a>	
									<?php if($pdp_get_finance != 1 || $pdp_get_finance == '' ){?>						
										<a href="<?php if($getfinancereplace ==1){ echo $getfinancereplaceurl ;}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button"><?php if($getfinancereplace =='1'){ echo $getfinancetext ;}else{ echo 'Financing'; } ?></a>	
									<?php } ?>		
								</div>
									
								<?php if(get_option('getcouponbtn') == 1){ ?>
									<a href="<?php echo $getcoupon_link ;?>" target="_self" class="button alt getcoupon-btn" role="button" >GET COUPON</a>
								<?php } ?>

							<?php  if($getcouponreplace == 1 && $getcouponreplaceurl !='' && $getcouponreplacetext !=''){ ?>

								<a href="<?php echo $getcouponreplaceurl ;?>" target="_self" class="button alt custompdpbtn getcoupon-btn" role="button">
								<?php if($getcouponreplace ==1){ echo $getcouponreplacetext ;}?>
								</a>

							<?php } ?>	
									
								</div>						
									<div class="clearfix"></div>
							</div>
								<div class="clearfix"></div>
						</div>
						
						<div class="clearfix"></div>
						
					</div>
				</article>
				<?php
				
			endwhile;
			else :
			endif;
	?>
</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>